package com.homework.textTransformation;

import java.io.*;
import java.util.Scanner;

/**
 * Created by Valeriy on 28.10.2016.
 */
public class TextTransformation {
    public static void Transformation(String filename1,String filename2) throws IOException {
        InputStream is = new FileInputStream(filename1);
        Scanner sc = new Scanner(is);
        FileWriter wr = new FileWriter(filename2);
        System.out.println("Result: ");
        while (sc.hasNext()){
            String st = sc.next().toUpperCase();
            if(st.length()%2==0)
            {
                wr.write(st);
                System.out.print(st);
                if(sc.hasNext()) {
                    wr.write(", ");
                    System.out.print(", ");
                }
            }

        }
        sc.close();
        wr.close();
    }
    public static void main(String[] args) throws IOException {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter source, destination:");
        Transformation("src\\main\\java\\com\\homework\\textTransformation\\Not_empty.txt",//+ scanner.nextLine(),
                "src\\main\\java\\com\\homework\\textTransformation\\Empty.txt");//+ scanner.nextLine());
        scanner.close();
    }
}
