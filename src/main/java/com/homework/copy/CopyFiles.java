package com.homework.copy;

import java.io.*;
import java.util.Scanner;

/**
 * Created by Valeriy on 26.10.2016.
 */
public class CopyFiles {
    private static void Copy(String source,String dest) throws IOException {
        InputStream is = new FileInputStream(dest);
        OutputStream os = new FileOutputStream(source);
        byte arr[] = new byte[is.available()];
        int num = is.read(arr);
        os.write(arr);
            System.out.println(num+" bytes copied");
        is.close();
        os.close();
    }

    public static void main(String[] args) throws IOException {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter destination,source:");
        Copy("src\\main\\java\\com\\homework\\copy\\"+ scanner.nextLine(),
                "src\\main\\java\\com\\homework\\copy\\"+scanner.nextLine());
        scanner.close();
    }
}
