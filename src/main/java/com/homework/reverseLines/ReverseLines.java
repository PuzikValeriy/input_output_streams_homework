package com.homework.reverseLines;

import java.io.*;
import java.util.Scanner;

/**
 * Created by Valeriy on 28.10.2016.
 */
public class ReverseLines {
    public static void Reverse(String filename1, String filename2) throws IOException {
        InputStream is = new FileInputStream(filename1);
        Scanner sc = new Scanner(is);
        FileWriter fw = new FileWriter(filename2);
        BufferedWriter bw = new BufferedWriter(fw);

        System.out.println("Result:");
        while (sc.hasNext()){
            String st = sc.next();
            st = new StringBuilder(st).reverse().toString();
            bw.write(st);
            if(sc.hasNext()) {
                bw.write(" ");
                System.out.print(st+" ");}
        }
        is.close();
        fw.close();
        bw.flush();
    }
    public static void main(String[] args) throws IOException {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter source,destination:");
        Reverse("src\\main\\java\\com\\homework\\reverseLines\\"+ scanner.nextLine(),
                "src\\main\\java\\com\\homework\\reverseLines\\"+ scanner.nextLine());
        scanner.close();
    }
}
