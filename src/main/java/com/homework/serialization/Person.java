package com.homework.serialization;

import java.io.*;

/**
 * Created by Valeriy on 28.10.2016.
 */
public class Person implements Serializable{
    transient String firstName;
    transient String lastName;
    String fullName;
    final String finalString;
    Sex sex;
    transient PrintStream outputStream;
    private static final long serialVersionUID = 42L;

    Person(String firstName, String lastName,  Sex sex) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.fullName = String.format("%s, %s", lastName, firstName);
        this.finalString = "final string";
        this.sex = sex;
        this.outputStream = System.out;
    }

    @Override
    public String toString() {
        return firstName + " " +
                lastName + ", " +
                fullName + " " +
                sex + " " +
                finalString + "\n";
    }

    private void writeObject(ObjectOutputStream out) throws IOException {
        out.defaultWriteObject();
    }

    private void readObject(ObjectInputStream in) throws IOException, ClassNotFoundException {
        in.defaultReadObject();
        String[] names = this.fullName.split(",");
        this.lastName=names[0];
        this.firstName=names[1];
        this.outputStream=System.out;
    }

    public static void main(String[] args) throws IOException, ClassNotFoundException {
        FileOutputStream fs = new FileOutputStream("src\\main\\java\\com\\homework\\serialization\\Empty.txt");
        ObjectOutputStream oos = new ObjectOutputStream(fs);
        Person student = new Person("Andrew","Queen",Sex.MALE);
        Person teacher = new Person("Olga","Smoke",Sex.FEMALE);
        Person newStudent,newTeacher;
        oos.writeObject(student);
        oos.writeObject(teacher);
        oos.close();
        ObjectInputStream ois = new ObjectInputStream(
                new FileInputStream("src\\main\\java\\com\\homework\\serialization\\Empty.txt"));
        newStudent=(Person)ois.readObject();
        newTeacher=(Person)ois.readObject();
        System.out.println("Serialisation done. Deserialized result:");
        System.out.println(newStudent.toString());
        System.out.println(newTeacher.toString());
        ois.close();
        oos.close();
    }
}
