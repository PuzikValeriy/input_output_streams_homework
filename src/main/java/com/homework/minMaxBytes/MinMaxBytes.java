package com.homework.minMaxBytes;

import java.io.*;
import java.util.Scanner;

/**
 * Created by Valeriy on 26.10.2016.
 */
public class MinMaxBytes {
    public static void FindBytes(String source) throws IOException {
        InputStream is = new FileInputStream(source);
        Reader reader = new InputStreamReader(is);
        char arr[] = new char[is.available()];
        int count=0,min=Integer.MAX_VALUE,max=Integer.MIN_VALUE;
        char minChar='0',maxChar='0';
        reader.read(arr);
            for (int i = 'A'; i <= 'z'; i++) {
                for (int t=0;t<arr.length;t++) {
                    if (arr[t] == i)
                        count++;
                }
                if(count<min&&count>0) {
                    minChar = (char)i;
                    min=count;
                }
                if(count>max){
                    maxChar=(char)i;
                    max=count;
                }
                count = 0;
            }
        System.out.println(arr);
        System.out.println("Minimum: ["+ minChar+"] = "+(int)minChar+". Number of repeats: "+ min);
        System.out.println("Maximum: ["+ maxChar+"] = "+(int)maxChar+". Number of repeats: "+ max);
        is.close();
        reader.close();
    }
    public static void main(String[] args) throws IOException {
        System.out.println("Enter filename");
        Scanner scanner = new Scanner(System.in);
        FindBytes("src\\main\\java\\com\\homework\\minMaxBytes\\"+scanner.nextLine());
        scanner.close();
    }
}
