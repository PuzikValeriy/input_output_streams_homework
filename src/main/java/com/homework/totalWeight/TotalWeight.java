package com.homework.totalWeight;

import java.io.*;
import java.util.*;

/**
 * Created by Valeriy on 26.10.2016.
 */
public class TotalWeight {
    public static void SummaryWeight(String filename) throws IOException {
        Map<String, Integer> lines = new HashMap<>();
        InputStream is = new FileInputStream(filename);
        Scanner sc = new Scanner(is);
        String key,keyMaxValue = null;
        Integer value,maxValue=Integer.MIN_VALUE;

        while (sc.hasNext()){
            key = sc.next();
            value=sc.nextInt();
            if(lines.containsKey(key))
            {
                value=lines.get(key)+value;
                lines.put(key,value);
            }
            else lines.put(key,value);
            if(value>maxValue){
                keyMaxValue=key;
            }
        }
        System.out.println(keyMaxValue+" "+lines.get(keyMaxValue)+" kg");
        sc.close();
        is.close();
    }
    public static void main(String[] args) throws IOException {
        System.out.println("Enter filename");
        Scanner scanner = new Scanner(System.in);
        SummaryWeight("src\\main\\java\\com\\homework\\totalWeight\\"+scanner.nextLine());
        scanner.close();
    }
}
