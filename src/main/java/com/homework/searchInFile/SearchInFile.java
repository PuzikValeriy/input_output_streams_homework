package com.homework.searchInFile;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * Created by Valeriy on 26.10.2016.
 */
public class SearchInFile {
    public static void Search(String filename,String word) throws IOException {
        InputStream is = new FileInputStream(filename);
        Scanner scanner = new Scanner(is);
        List<String> searchedLines = new ArrayList<>();
        while (scanner.hasNextLine()){
            String line = scanner.nextLine();
            if(line.contains(word)){
                searchedLines.add(line);
            }
        }
        System.out.println(searchedLines);
        scanner.close();
        is.close();
    }
    public static void main(String[] args) throws IOException {
        System.out.println("Enter filename, key");
        Scanner scanner = new Scanner(System.in);
        Search("src\\main\\java\\com\\homework\\searchInFile\\"+ scanner.nextLine(), scanner.next());
        scanner.close();
    }
}
